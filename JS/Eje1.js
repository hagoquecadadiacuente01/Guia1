//-------------Variables--------------
//Captura la fecha actual
var d = new Date();
//Utilizada como variable prueba en proceso del If
var yo = d.getDay();
//array's con los días de semana y meses del año 
var sema = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
var mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
document.write('<div class="Datos">')
document.write('<p>Fecha : '+sema[d.getDay()]+' , '+d.getDate() + " de " + mes[d.getMonth()] + " de " + d.getFullYear())
document.write('</p></div>')

//-------------------Frase del dia------------
document.write('<div class="enunciado1">');
if(yo==1){
//---------------Si es lunes----------------------
    document.write('<p>Donde no falta voluntad siempre hay un camino. </p>')
} else if(yo == 2) {
//----------------Si es martes--------------------	
    document.write('<p>Todo fracaso es el condimento que da sabor al éxito.</p>')
}else if (yo == 3) {
//-----------------Si es miercoles-----------------
    document.write('<p>Encuéntrate y sé tú mismo; recuerda que no hay nadie como tú.</p>')
}else if (yo == 4) {
//-----------------Si es jueves--------------------
    document.write('<p>Confía en el tiempo, que suele dar dulces salidas a muchas amargas dificultades.</p>')
}else if (yo == 5) {
//-----------------Si es viernes-------------------
    document.write('<p>La cosa más importante que debes hacer si estás dentro de un hoyo es dejar de cavar.</p>')
} else if (yo == 6) {
//-----------------Si es sabado-------------------
    document.write('<p>Es duro fracasar, pero es todavía peor no haber intentado nunca triunfar.</p>')
}else{
//----------------O si es domingo------------------
    document.write('<p>Nuestra mayor debilidad es rendirse, la única manera de tener éxito es intentarlo siempre una vez más.</p>')
}
document.write('</div>');

//-------------Fotos------------------------

document.write('<div class="enunciado2">');
if(yo==1){
//---------------Si es lunes----------------------
    document.write('<img src="../IMG/Lu.jpg">')
} else if(yo == 2) {
//----------------Si es martes--------------------	
    document.write('<img src="../IMG/Ma.jpg">')
}else if (yo == 3) {
//-----------------Si es miercoles-----------------
    document.write('<img src="../IMG/Mi.jpg">')
}else if (yo == 4) {
//-----------------Si es jueves--------------------
    document.write('<img src="../IMG/Ju.jpg">')
}else if (yo == 5) {
//-----------------Si es viernes-------------------
    document.write('<img src="../IMG/Vi2.jpg">')
} else if (yo == 6) {
//-----------------Si es sabado-------------------
    document.write('<img src="../IMG/Sa.jpg">')
}else{
//----------------O si es domingo------------------
    document.write('<img src="../IMG/Do.jpg>')
}
document.write('</div>');
    
document.write(' ');